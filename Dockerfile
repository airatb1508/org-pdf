FROM archlinux:base-devel

RUN useradd --no-create-home --shell=/bin/false build && usermod -L build
RUN echo "build ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER build
RUN sudo pacman -Syu --noconfirm git texlive-most inkscape emacs
RUN git clone https://aur.archlinux.org/otf-xits.git /tmp/otf-xits
RUN cd /tmp/otf-xits && makepkg -si --noconfirm

CMD cd /org && make re
