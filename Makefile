SRC=text.org
NAME=text.pdf

all: $(NAME)

$(NAME): $(SRC)
	emacs $< --batch -f org-latex-export-to-pdf --kill

clean:
	rm -f ./text.tex* ./*.aux ./*.log ./*.out

fclean: clean
	rm -f ./text.pdf

re: fclean all
